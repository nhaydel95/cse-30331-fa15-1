Milestone 2
===========

Due: 2015/09/24 at 11:59pm
Points: 2

2a. By now, you should have committed to an application to work on.

Talk about the community of developers for your application. How
active is the community discussion forum? Are there regular stable
releases of the source code? Is there a formal mechanism to submit
bugs?  Describe how the code is structured: for example, how large is
it (lines of code), what language(s) is it written in, what
platform(s) is it written for, what external library dependencies are
there?

2b. You should also be able to successfully compile and run your
chosen application.

Describe the environment (hardware, operating system, compiler) where
you will do your development. You can use a virtual machine like
VirtualBox or VMWare Player. If you have successfully compiled and run
your application, state the date that your team first did so. If not,
please explain in detail the challenges you are having, and how you
plan to overcome them. What errors do you get? Are there tests to help
you verify you have it working correctly? Have you contacted the
software's development team? Have you met with TAs?