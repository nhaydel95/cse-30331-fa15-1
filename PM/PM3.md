Milestone 3
===========

Due: 2015/10/08 at 11:59pm
Points: 2

3a. By now, each team member should have selected something specific
to work on. It could be:

- to add a new feature,
- to optimize a function (for time or memory efficiency), or
- to fix a problem.

In any case, it should relate somehow to a data structure or algorithm
covered in class. For each,

- State who will work on this piece.
- Describe what you want to do. If it's a new feature: What is it and
  why is it needed? If it's something inefficient or broken: What is
  it and how is it currently implemented? What is its asymptotic
  time/space complexity? What's wrong with it?
- Describe how this piece interfaces (or will interface) with the rest
  of the code. If it's a function, what are the pre- and
  post-conditions? If it's a class, what are the pre- and
  post-conditions of its public member functions?

3b. You should also be in contact with the developer community (ask
your TA for tips on how to do this successfully). Cut and paste an
excerpt from one of your conversations.
