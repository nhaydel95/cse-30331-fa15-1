Milestone 4
===========

Due: 2015/11/05 at 11:59pm
Points: 2

4a. For each modification, you should have an idea that you plan to
implement. Describe your idea, and its expected performance
(asymptotic time/space complexity and/or practical time/space usage).

4b. How is your team managing the project source code? Are you working
on multiple computers? Do you have a shared repository? Are you using
branches to make experimental changes to the code?
