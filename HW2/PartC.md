Homework 2 - Part C
===================
DUE 9/22 at the beginning of the class

5. What are the three main steps in the quicksort method? 


6. Which of these statements is NOT true of mergesort and its variations?
A. Merge sort is only used as an external sorting algorithm. 
B. Merge sort is usually implemented recursively. 
C. The worst-case performance for merge sort is O(n log n).
D. Binary merge sort is used on subfiles that have a size that is a power of 2.